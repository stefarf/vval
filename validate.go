package vval

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/stefarf/vval/vrule"
)

const maxValueLen = 50

type Map map[string]vrule.Fn

func New() Map {
	return Map{
		"emptyOk":      vrule.EmptyOk(),
		"skip":         vrule.Skip(),
		"timeNotEmpty": vrule.TimeNotEmpty(),
		"validJson":    vrule.ValidJson(),
	}
}

func (vm Map) AddRule(name string, fn vrule.Fn) Map {
	vm[name] = fn
	return vm
}

func (vm Map) Validate(name string, val any) error {
	var errs []string
	errs = vm.validateAny(errs, name, reflect.ValueOf(val), vrule.EmptyNotOk())
	if len(errs) == 0 {
		return nil
	}
	return fmt.Errorf("errors:\n%s", strings.Join(errs, "\n"))
}

func (vm Map) validateAny(errs []string, name string, v reflect.Value, fn vrule.Fn) []string {
	next, valStrIfErr, err := fn(v.Interface())
	if err != nil {
		if len(valStrIfErr) > maxValueLen {
			valStrIfErr = valStrIfErr[:maxValueLen-3] + " .."
		}
		return append(errs, fmt.Sprintf("- %s=%s -> %s", name, valStrIfErr, err.Error()))
	}
	if !next {
		return errs
	}

	switch v.Kind() {
	case reflect.Array:
		return vm.validateArray(errs, name, v, fn)
	case reflect.Interface:
		return vm.validateAny(errs, name, v, fn)
	case reflect.Map:
		return vm.validateMap(errs, name, v, fn)
	case reflect.Pointer:
		return vm.validatePointer(errs, name, v, fn)
	case reflect.Slice:
		return vm.validateSlice(errs, name, v, fn)
	case reflect.Struct:
		return vm.validateStruct(errs, name, v, fn)
	default:
		return errs
	}
}

func (vm Map) validateArray(errs []string, name string, v reflect.Value, fn vrule.Fn) []string {
	if v.Len() == 0 {
		return errs
	}
	return vm.validateEach(errs, name, v, fn)
}

func (vm Map) validateEach(errs []string, name string, v reflect.Value, fn vrule.Fn) []string {
	for i := 0; i < v.Len(); i++ {
		itemName := joinIndex(name, i)
		itemValue := v.Index(i)
		errs = vm.validateAny(errs, itemName, itemValue, fn)
	}
	return errs
}

func (vm Map) validateMap(errs []string, name string, v reflect.Value, fn vrule.Fn) []string {
	if v.IsNil() || v.Len() == 0 {
		return errs
	}
	iter := v.MapRange()
	for iter.Next() {
		mk := iter.Key()
		mv := iter.Value()
		errs = vm.validateAny(errs, joinMapKey(name, mk.String()), mv, fn)
	}
	return errs
}

func (vm Map) validatePointer(errs []string, name string, v reflect.Value, fn vrule.Fn) []string {
	if v.IsNil() {
		return errs
	}
	return vm.validateAny(errs, name, v.Elem(), fn)
}

func (vm Map) validateSlice(errs []string, name string, v reflect.Value, fn vrule.Fn) []string {
	if v.IsNil() || v.Len() == 0 {
		return errs
	}
	return vm.validateEach(errs, name, v, fn)
}

func (vm Map) validateStruct(errs []string, name string, v reflect.Value, fn vrule.Fn) []string {
	t := v.Type()
	for i := 0; i < t.NumField(); i++ {
		fieldValue := v.Field(i)
		fieldName := joinDot(name, t.Field(i).Name)

		if !t.Field(i).IsExported() {
			continue
		}

		fieldFn := fn
		valTag := t.Field(i).Tag.Get("val")
		if valTag != "" {
			fieldFn = vm[valTag]
		}
		if fieldFn == nil {
			errs = append(errs, fmt.Sprintf("- %s -> invalid val tag '%s'", fieldName, valTag))
			continue
		}

		errs = vm.validateAny(errs, fieldName, fieldValue, fieldFn)
	}
	return errs
}
