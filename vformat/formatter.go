package vformat

import (
	"fmt"
)

func Default(v any) string  { return fmt.Sprintf("%v", v) }
func Quote(s string) string { return fmt.Sprintf("\"%s\"", s) }
