package vval

import (
	"fmt"
)

func joinDot(name, fieldName string) string {
	return fmt.Sprintf("%s.%s", name, fieldName)
}

func joinMapKey(name, key string) string {
	return fmt.Sprintf("%s[%s]", name, key)
}

func joinIndex(name string, index int) string {
	return fmt.Sprintf("%s[%d]", name, index)
}
