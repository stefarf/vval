package vrule

import (
	"fmt"
	"reflect"

	"gitlab.com/stefarf/vval/vformat"
)

func StringEnum(ss ...string) Fn {
	validate := func(v string) (next bool, valStrIfErr string, err error) {
		for _, s := range ss {
			if v == s {
				return validValue()
			}
		}
		return invalidValue(vformat.Quote(v), fmt.Errorf("value must be one of %v", ss))
	}
	return func(val any) (next bool, valStrIfErr string, err error) {
		if nonTerminalKind(reflect.ValueOf(val)) {
			return nextValue()
		}

		switch v := val.(type) {
		case string:
			return validate(v)
		default:
			return invalidValue(vformat.Default(val), errInvalidType)
		}
	}
}
