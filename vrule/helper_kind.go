package vrule

import "reflect"

func nonTerminalKind(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Array,
		reflect.Interface,
		reflect.Map,
		reflect.Pointer,
		reflect.Slice,
		reflect.Struct:
		return true
	default:
		return false
	}
}

func pointerKind(v reflect.Value) bool { return v.Kind() == reflect.Pointer }
