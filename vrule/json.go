package vrule

import (
	"encoding/json"
	"reflect"

	"gitlab.com/stefarf/vval/vformat"
)

func ValidJson() Fn {
	validate := func(b []byte) (next bool, valStrIfErr string, err error) {
		if !json.Valid(b) {
			return invalidValue(string(b), errInvalidJson)
		}
		return validValue()
	}
	return func(val any) (next bool, valStrIfErr string, err error) {
		if pointerKind(reflect.ValueOf(val)) {
			return nextValue()
		}

		switch b := val.(type) {
		case json.RawMessage:
			return validate(b)
		case []byte:
			return validate(b)
		case string:
			return validate([]byte(b))
		default:
			return invalidValue(vformat.Default(val), errNotJson)
		}
	}
}
