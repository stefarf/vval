package vrule

import (
	"fmt"
	"reflect"

	"gitlab.com/stefarf/vval/vformat"
)

func valueToInt(v reflect.Value) (int64, error) {
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int(), nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return int64(v.Uint()), nil
	default:
		return 0, errUnexpectedKind
	}
}

func validateInt(
	val any,
	validate func(v int64) (next bool, valStrIfErr string, err error),
) (next bool, valStrIfErr string, err error) {
	v := reflect.ValueOf(val)
	if nonTerminalKind(v) {
		return nextValue()
	}
	i, err := valueToInt(v)
	if err != nil {
		return invalidValue(vformat.Default(val), err)
	}
	return validate(i)
}

func IntBetween(a, b int64) Fn {
	validate := func(v int64) (next bool, valStrIfErr string, err error) {
		if a <= v && v <= b {
			return validValue()
		}
		return invalidValue(vformat.Default(v), fmt.Errorf("value must be between %d and %d", a, b))
	}
	return func(val any) (next bool, valStrIfErr string, err error) { return validateInt(val, validate) }
}

func IntEnum(ii ...int64) Fn {
	validate := func(v int64) (next bool, valStrIfErr string, err error) {
		for _, i := range ii {
			if v == i {
				return validValue()
			}
		}
		return invalidValue(vformat.Default(v), fmt.Errorf("value must be one of %v", ii))
	}
	return func(val any) (next bool, valStrIfErr string, err error) { return validateInt(val, validate) }
}

func IntGreaterThanOrEqual(i int64) Fn {
	validate := func(v int64) (next bool, valStrIfErr string, err error) {
		if v >= i {
			return validValue()
		}
		return invalidValue(vformat.Default(v), fmt.Errorf("value must be greater than or equal %d", i))
	}
	return func(val any) (next bool, valStrIfErr string, err error) { return validateInt(val, validate) }
}
