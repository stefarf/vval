package vrule

import (
	"reflect"

	"gitlab.com/stefarf/vval/vformat"
)

func EmptyNotOk() Fn {
	return func(val any) (next bool, valStrIfErr string, err error) {
		v := reflect.ValueOf(val)
		switch v.Kind() {
		case reflect.Array,
			reflect.Interface,
			reflect.Map,
			reflect.Pointer,
			reflect.Slice,
			reflect.Struct:
			return nextValue()

		case reflect.Bool:

		case reflect.Float32, reflect.Float64:
			if v.Float() == 0 {
				return invalidValue("0", errEmptyValue)
			}

		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			if v.Int() == 0 {
				return invalidValue("0", errEmptyValue)
			}

		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			if v.Uint() == 0 {
				return invalidValue("0", errEmptyValue)
			}

		case reflect.String:
			if v.String() == "" {
				return invalidValue("\"\"", errEmptyValue)
			}

		default:
			return invalidValue(vformat.Default(val), errUnexpectedKind)
		}
		return validValue()
	}
}

func EmptyOk() Fn {
	return func(val any) (next bool, valStrIfErr string, err error) { return nextValue() }
}

func Skip() Fn {
	return func(val any) (next bool, valStrIfErr string, err error) { return validValue() }
}
