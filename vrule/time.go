package vrule

import (
	"reflect"
	"time"

	"gitlab.com/stefarf/vval/vformat"
)

func TimeNotEmpty() Fn {
	return func(val any) (next bool, valStrIfErr string, err error) {
		if pointerKind(reflect.ValueOf(val)) {
			return nextValue()
		}

		t, ok := val.(time.Time)
		if !ok {
			return invalidValue(vformat.Default(val), errNotTimeStruct)
		}
		if t.Sub(time.Time{}) == 0 {
			return invalidValue(vformat.Quote(vformat.Default(val)), errEmptyValue)
		}
		return validValue()
	}
}
