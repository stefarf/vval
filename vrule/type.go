package vrule

type Fn func(val any) (next bool, valStrIfErr string, err error)
