package vrule

func invalidValue(valStr string, setErr error) (next bool, valStrIfErr string, err error) {
	return false, valStr, setErr
}

func nextValue() (next bool, valStrIfErr string, err error)  { return true, "", nil }
func validValue() (next bool, valStrIfErr string, err error) { return false, "", nil }
