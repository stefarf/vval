package vrule

import "errors"

var (
	errEmptyValue     = errors.New("empty value")
	errInvalidJson    = errors.New("invalid json")
	errInvalidType    = errors.New("invalid type")
	errNotJson        = errors.New("not a json compatible type")
	errNotTimeStruct  = errors.New("not a time.Time struct")
	errUnexpectedKind = errors.New("unexpected kind")
)
