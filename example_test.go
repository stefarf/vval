package vval_test

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/stefarf/vval"
	"gitlab.com/stefarf/vval/vrule"
)

var value struct {
	unexportedStruct struct {
		S string
	} // do not check unexported field
	unexportedInt int // do not check unexported field

	Empty struct {
		B bool // bool value can be true or false

		I  int     // empty int, int8, int16, int32, int64 is an error
		Ui uint64  // empty uint, uint8, uint16, uint32, uint64 is an error
		F  float32 // empty float32, float64 is an error

		F2 float32 `val:"checkFloat"` // run the checkFloat rule for this value, error if the rule is  not defined

		S  string // empty string is an error
		S2 string `val:"emptyOk"` // this field can be empty

		A [3]int // empty int, int8, int16, int32, int64 is an error

		O *struct {
			I int
		} // nil pointer is not an error
		O2 struct {
			Fruit string `val:"validFruit"` // skip checking because of O2 is skipped
		} `val:"skip"` // skip checking for this field and its subfields (child fields)
		O3 struct {
			S     string // this field can be empty because O3 can be empty
			Fruit string `val:"validFruit"` // run the validFruit rule for this value
		} `val:"emptyOk"` // this field can be empty, its subfields can also be empty

		Bytes []byte // nil value is not an error

		T  time.Time  `val:"timeNotEmpty"` // run the timeNotEmpty rule for this value
		T2 *time.Time `val:"timeNotEmpty"` // run the timeNotEmpty rule for this value if it is not nil pointer
		T3 time.Time  // not error because time.Time has no exported field to validate

		Raw        json.RawMessage  // nil value is not an error
		Raw2       json.RawMessage  `val:"validJson"` // run the validJson rule for this value
		Raw3       *json.RawMessage `val:"validJson"` // run the validJson rule for this value if it is not nil pointer
		JsonString string           `val:"validJson"` // run the validJson rule for this value
	}

	Valid struct {
		Fruit string          `val:"validFruit"`
		Age   uint            `val:"validAge"`
		Ages  []int32         `val:"validAge"`
		T     *time.Time      `val:"timeNotEmpty"`
		Raw   json.RawMessage `val:"validJson"`
		Raw2  json.RawMessage `val:"validJson"`
	}

	Invalid struct {
		Fruit string          `val:"validFruit"`
		Age   uint            `val:"validAge"`
		Ages  []int32         `val:"validAge"`
		Raw   json.RawMessage `val:"validJson"`
		Raw2  json.RawMessage `val:"validJson"`

		AgesStruct struct {
			Ages      []int32 // run the "validAge" (follow the struct rule) rule for this value
			OtherAges []int   `val:"otherAge"` // run the "otherAge" (overwrite the struct rule) rule for this value
		} `val:"validAge"` // run the "validAge" rule for this struct subfields

	}
}

func ExampleMap_Validate() {
	// set valid values

	value.Valid.Fruit = "orange"
	value.Valid.Age = 30
	value.Valid.Ages = []int32{0, 10, 70, 100}

	now := time.Now()
	value.Valid.T = &now

	value.Valid.Raw = json.RawMessage("\"\"")
	value.Valid.Raw2 = json.RawMessage("null")

	// set invalid values

	value.Invalid.Fruit = "banana"
	value.Invalid.Age = 110
	value.Invalid.Ages = []int32{-10, -1, 101}
	value.Invalid.AgesStruct.Ages = []int32{-1, 1000}
	value.Invalid.AgesStruct.OtherAges = []int{-1, 1000}
	value.Invalid.Raw = json.RawMessage("nil")
	value.Invalid.Raw2 = json.RawMessage("")

	// validate
	fmt.Println(vval.New().
		AddRule("validFruit", vrule.StringEnum("apple", "orange")).
		AddRule("validAge", vrule.IntBetween(0, 100)).
		AddRule("otherAge", vrule.IntEnum(10, 20, 30)).
		Validate("value", value))

	//Output:
	//errors:
	//- value.Empty.I=0 -> empty value
	//- value.Empty.Ui=0 -> empty value
	//- value.Empty.F=0 -> empty value
	//- value.Empty.F2 -> invalid val tag 'checkFloat'
	//- value.Empty.S="" -> empty value
	//- value.Empty.A[0]=0 -> empty value
	//- value.Empty.A[1]=0 -> empty value
	//- value.Empty.A[2]=0 -> empty value
	//- value.Empty.O3.Fruit="" -> value must be one of [apple orange]
	//- value.Empty.T="0001-01-01 00:00:00 +0000 UTC" -> empty value
	//- value.Empty.Raw2= -> invalid json
	//- value.Empty.JsonString= -> invalid json
	//- value.Invalid.Fruit="banana" -> value must be one of [apple orange]
	//- value.Invalid.Age=110 -> value must be between 0 and 100
	//- value.Invalid.Ages[0]=-10 -> value must be between 0 and 100
	//- value.Invalid.Ages[1]=-1 -> value must be between 0 and 100
	//- value.Invalid.Ages[2]=101 -> value must be between 0 and 100
	//- value.Invalid.Raw=nil -> invalid json
	//- value.Invalid.Raw2= -> invalid json
	//- value.Invalid.AgesStruct.Ages[0]=-1 -> value must be between 0 and 100
	//- value.Invalid.AgesStruct.Ages[1]=1000 -> value must be between 0 and 100
	//- value.Invalid.AgesStruct.OtherAges[0]=-1 -> value must be one of [10 20 30]
	//- value.Invalid.AgesStruct.OtherAges[1]=1000 -> value must be one of [10 20 30]
}
